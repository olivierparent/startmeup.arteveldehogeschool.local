<?php

namespace StartMeUp\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \StartMeUp\Console\Commands\ArteveldeDatabaseBackupCommand::class,
        \StartMeUp\Console\Commands\ArteveldeDatabaseDropCommand::class,
        \StartMeUp\Console\Commands\ArteveldeDatabaseInitCommand::class,
        \StartMeUp\Console\Commands\ArteveldeDatabaseResetCommand::class,
        \StartMeUp\Console\Commands\ArteveldeDatabaseRestoreCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
    }
}
