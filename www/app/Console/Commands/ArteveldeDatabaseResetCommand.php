<?php

namespace StartMeUp\Console\Commands;

use Illuminate\Console\Command;

class ArteveldeDatabaseResetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artevelde:database:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drops database and runs artevelde:database:init';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get variables from .env
        $db_name = getenv('DB_DATABASE');

        // Drop database and initialize
        $this->callSilent('artevelde:database:drop');
        $this->callSilent('artevelde:database:init');

        $this->comment("Database `${db_name}` reset!");
    }
}
