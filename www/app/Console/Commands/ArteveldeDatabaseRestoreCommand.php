<?php

namespace StartMeUp\Console\Commands;

use Illuminate\Console\Command;

class ArteveldeDatabaseRestoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artevelde:database:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restores database from latest SQL dump';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get variables from .env
        $db_name = getenv('DB_DATABASE');
        $db_user = getenv('DB_USERNAME');
        $db_pass = getenv('DB_PASSWORD');
        $db_dump = getcwd().'/'.getenv('DB_DUMP_PATH');

        // Reset database
        $this->callSilent('artevelde:database:reset');

        // Restore SQL dump
        $command = "MYSQL_PWD=${db_pass} mysql --user=${db_user} ${db_name} < ${db_dump}/latest.sql";
        exec($command);

        $this->comment("Backup for database `${db_name}` restored!");
    }
}
