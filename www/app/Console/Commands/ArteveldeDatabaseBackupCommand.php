<?php

namespace StartMeUp\Console\Commands;

use Illuminate\Console\Command;

class ArteveldeDatabaseBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artevelde:database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dumps database to SQL file for backup';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get variables from .env
        $db_name = getenv('DB_DATABASE');
        $db_user = getenv('DB_USERNAME');
        $db_pass = getenv('DB_PASSWORD');
        $db_dump = getcwd().'/'.getenv('DB_DUMP_PATH');

        // Create directory
        $command = "mkdir -p ${db_dump}";
        exec($command);

        // Create SQL dump
        $command = "MYSQL_PWD=${db_pass} mysqldump --user=${db_user} --databases ${db_name} > ${db_dump}/latest.sql";
        exec($command);

        // Gzip created SQL dump
        $command = "gzip -cr ${db_dump}/latest.sql > ${db_dump}/$(date +\"%Y-%m-%d_%H%M%S\").sql.gz";
        exec($command);

        $this->comment("Database `${db_name}` stored!");
    }
}
