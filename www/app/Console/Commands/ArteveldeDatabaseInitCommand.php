<?php

namespace StartMeUp\Console\Commands;

use Illuminate\Console\Command;

class ArteveldeDatabaseInitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artevelde:database:init {--seed : Run migrations and seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates database user and database, and executes Doctrine migrations';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get variables from .env
        $db_name = getenv('DB_DATABASE');
        $db_user = getenv('DB_USERNAME');
        $db_pass = getenv('DB_PASSWORD');

        // Grant privileges
        $sql = "GRANT ALL PRIVILEGES ON ${db_name}.* TO '${db_user}' IDENTIFIED BY '${db_pass}'";
        $command = sprintf('MYSQL_PWD=%s mysql --user=%s --execute="%s"', 'secret', 'homestead', $sql);
        exec($command);

        // Create database
        $sql = "CREATE DATABASE IF NOT EXISTS ${db_name} CHARACTER SET utf8 COLLATE utf8_general_ci";
        $command = sprintf('MYSQL_PWD=%s mysql --user=%s --execute="%s"', $db_pass, $db_user, $sql);
        exec($command);

        // Run migrations
        if ($this->option('seed')) {
            $this->call('migrate', [
                '--seed' => true,
            ]);
        }

        $this->comment("Database `${db_name}` initalized!");
    }
}
