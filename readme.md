StartMeUp app
=============


[TOC]


I. About
--------

This app is made as a proof of concept by [Artevelde University College Ghent][artevelde] as part of a project-based scientific research project.

### Project Team

 - Karijn Bonne       (Lecturer/Researcher)
 - Christel De Maeyer (Lecturer/Researcher, project lead)
 - Olivier Parent     (Lecturer/Researcher, design and development)

### Contributors

 - Lies Van Assche (Student, graphic design)
 - Orphée Alliet   (Student, graphic design)

II. Installation
----------------

### Local Development Environment

 - Use [Artevelde Laravel Homestead][artestead].

#### Configuration 

    $ artestead edit
 
```
sites:

…

# StartMeUp
# ---------

    - map: www.startmeup.arteveldehogeschool.local
      to: /home/vagrant/Code/startmeup.arteveldehogeschool.local/www/public
      hhvm: false
```

Start with the `provision` option to enable the new configuration option

    $ artestead up --provision

#### Install Dependencies

1. Clone the project and go to the folder.


    $ cd ~/Code/
    $ git clone http://bitbucket.org/olivierparent/startmeup.arteveldehogeschool.local
    $ cd ~/Code/startmeup.arteveldehogeschool.local/www/


2. Update Composer


    $ composer self-update


3. Install the Composer packages (`composer.lock`) and then update them


    $ composer install
    $ composer update


4. Install Node packages


    $ npm install


5. Install Bower packages


    $ bower install


6. Login to the Artevelde Laravel Homestead sever via SSH


    $ artestead ssh


7. Copy the default environment settings file.


    vagrant@homestead$ cd ~/Code/startmeup.arteveldehogeschool.local/www/
    vagrant@homestead$ cp .env.example .env


> Windows:  
> Convert the MS-DOS line endings to Unix line endings so the files can be properly executed. 
>
>      vagrant@homestead$ dos2unix **/.settings **/*.sh artisan


8. Initialize the database (create the database) and then run the Migrations (create database tables) and Seeders (populate the database tables with data)


    vagrant@homestead$ cd ~/Code/startmeup.arteveldehogeschool.local/www/
    vagrant@homestead$ ./artisan artevelde:database:init --seed

### Pages

 - **API**
    - http://www.startmeup.arteveldehogeschool.local/api/v1
 - **Backoffice**
    - http://www.startmeup.arteveldehogeschool.local/backoffice
 - **Frontoffice**
    - http://www.startmeup.arteveldehogeschool.local
    - http://www.startmeup.arteveldehogeschool.local/frontoffice#/gamification
    - http://www.startmeup.arteveldehogeschool.local/frontoffice#/goals
    - http://www.startmeup.arteveldehogeschool.local/frontoffice#/moods
    - http://www.startmeup.arteveldehogeschool.local/frontoffice#/nearby
    - http://www.startmeup.arteveldehogeschool.local/frontoffice#/settings
 - **Style Guide**
    - http://www.startmeup.arteveldehogeschool.local/styleguide

[artevelde]:    http://www.arteveldehogeschool.be/en
[artestead]:    https://github.com/OlivierParent/homestead
